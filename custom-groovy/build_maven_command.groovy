import it.csi.alm.Constants

def exampleMethod(build_map) {
    jdk = Constants.buildEnvironmentVersions.versions.jdk[build_map.artifactBuildEnvironment]
    mvn = Constants.buildToolVersions.versions.maven[build_map.artifactBuildToolVersion]
    withEnv(["JAVA_HOME=${jdk}"]) {
        sh "${mvn}mvn clean install -DskipTests=true"
    }
}

return this